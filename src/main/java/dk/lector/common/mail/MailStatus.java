package dk.lector.common.mail;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MailStatus {
	private String id;
	private String to;
	private String subject;
	//private String body;
	private String objectId;
	private String context;
	private Date sentDate = new Date();
	private String finalState;
	private String failedEmails;
	private MailServerStatus serverStatus;
	@JsonProperty(value=".priority")
	private long priority = new Date().getTime()*-1;
}
