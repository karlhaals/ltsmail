package dk.lector.common.mail;

import java.util.List;

public interface MailStatusRepository {

	void save(MailStatus build);
	
	MailStatus getStatus(String context, String id);

	List<MailStatus> getMailStatus(String context, String objectId);

	void create(MailStatus status);

}
