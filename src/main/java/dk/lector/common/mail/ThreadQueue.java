package dk.lector.common.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import lombok.Data;
import lombok.SneakyThrows;

@Data
public class ThreadQueue {

	private List<Runnable> commands;
	private int MAX = 5;

	public static ThreadQueue queue(int max) {
		ThreadQueue retur = new ThreadQueue();
		retur.setMAX(max);
		return retur;
	}

	public ThreadQueue command(Runnable command) {
		if (commands == null) {
			commands = new ArrayList<Runnable>();
		}
		commands.add(command);
		return this;
	}

	@SneakyThrows
	public void run(boolean wait) {
		ExecutorService es = Executors.newCachedThreadPool();// newFixedThreadPool(MAX);
		
		int thread = 0;
		for (Runnable command : commands) {
			if (thread++ == MAX) {
				es.shutdown();
				es.awaitTermination(3, TimeUnit.MINUTES);
				es = Executors.newCachedThreadPool();
				thread=0;
			}
			es.execute(command);

		}
	
		if (wait) {
			es.shutdown();
			es.awaitTermination(3, TimeUnit.MINUTES);
			es = null;
		}
		

	}

}
