package dk.lector.common.mail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MailServerStatus {
	private String status;
	private String recipients;
	private String delivered;
	private String failed;
	private String pending;
	private String opened;
	private String clicked;
	private String unsubscribed;
	private String abusereports;
}
