package dk.lector.common.mail;

import java.util.ArrayList;
import java.util.List;

public class MailBuilder {
	private String from;
	private String to;
	private String subject;
	private String body;
	private String nonHtmlBody;
	private String replyTo;
	private String cc;
	private String bcc;
	private List<Attachment> attachments;
	private String context;
	private String objectId;
	private MailBuilder(){}
	
	public MailBuilder bcc(String bcc){
		this.bcc = bcc;
		return this;
	}
	public MailBuilder cc(String cc){
		this.cc = cc;
		return this;
	}
	public MailBuilder replyTo(String replyTo){
		this.replyTo = replyTo;
		return this;
	}
	public static MailBuilder start(){
		return new MailBuilder();
	}
	
	public MailBuilder nonHtmlBody(String nonHtmlBody){
		this.nonHtmlBody = nonHtmlBody;
		return this;
	}
	
	public MailBuilder from(String from){
		this.from = from;
		return this;
	}
	public MailBuilder to(String to){
		this.to = to;
		return this;
	}
	public MailBuilder subject(String subject){
		this.subject = subject;
		return this;
	}
	public MailBuilder body(String body){
		this.body = body;
		return this;
	}
	
	public MailDTO build(){
		MailDTO mail = new MailDTO();
		mail.setFrom(from);
		mail.setTo(to);
		mail.setSubject(subject);
		mail.setBody(body);
		mail.setAttachments(attachments);
		mail.setNonHtmlBody(nonHtmlBody);
		mail.setReplyTo(replyTo);
		mail.setContext(context);
		mail.setObjectId(objectId);
		mail.setCc(cc);
		mail.setBcc(bcc);
		return mail;
	}

	public MailBuilder attachment(String name, byte[] data) {
		if(attachments==null){
			attachments = new ArrayList<Attachment>();
		}
		attachments.add(Attachment.builder().data(data).name(name).build());
		return this;
	}

	public MailBuilder context(String context) {
		this.context = context;
		return this;
	}

	public MailBuilder objectId(String objectId) {
		this.objectId = objectId;
		return this;
	}
	
	

}
