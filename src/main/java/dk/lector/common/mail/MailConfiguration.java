package dk.lector.common.mail;

import lombok.Data;

@Data
public class MailConfiguration {
	private String url;
	private String username;
	private String password;
	private int port;

}
