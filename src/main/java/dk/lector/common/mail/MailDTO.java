package dk.lector.common.mail;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor  
@AllArgsConstructor
public class MailDTO {
	private String subject;
	private String to;
	private String cc;
	private String bcc;
	private String from;
	private String body;
	private String nonHtmlBody;
	private String replyTo;
	private List<Attachment> attachments;
	private List<String> attachmentIds;
	private String objectId;
	private String context;
	public void addAttachmentId(String attachmentId) {
		if(attachmentIds==null){
			attachmentIds = new ArrayList<String>();
		}
		attachmentIds.add(attachmentId);
		
	}

	 
}
