package dk.lector.common.mail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.SneakyThrows;
import lombok.val;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(value = "singleton")
public class MailService {
	private static final String apiUrlStart = "https://api.elasticemail.com/";
	private static final String apiUrl = apiUrlStart
			+ "#resource#/#cmd#?username=#username#&api_key=#apiKey#";

	@Autowired
	private MailConfiguration configuration;
	@Autowired
	private MailStatusRepository repository;

	private List<MailDTO> mailList = new ArrayList<MailDTO>();

	public void queueMail(MailDTO mail) {
		mailList.add(mail);
	}

	public MailBuilder builder() {
		return MailBuilder.start();
	}

	public void sendEmails() {
		List<MailDTO> mails = mailList;
		mailList = new ArrayList<MailDTO>();
		// Map<String, String> attachmentMap = uploadAttachments(mails);
		val queue = ThreadQueue.queue(5);
		for (final MailDTO mail : mails) {
			/*
			 * for (Attachment attachement : mail.getAttachments()) { String
			 * attachmentId = attachmentMap.get(attachement .getChecksum());
			 * mail.addAttachmentId(attachmentId);
			 * 
			 * }
			 */
			queue.command(new Runnable() {

				@Override
				public void run() {
					sendEmail(mail);

				}
			});

		}
		queue.run(true);

	}

	private Map<String, String> uploadAttachments(List<MailDTO> mails) {
		final Map<String, String> attachmentMap = new HashMap<String, String>();
		Map<String, Attachment> map = new HashMap<String, Attachment>();
		for (MailDTO mail : mails) {
			List<Attachment> attachements = mail.getAttachments();
			for (Attachment att : attachements) {
				try {

					String checksum = Math.random() + "_"
							+ new Date().getTime();// checksum(att);
					att.setChecksum(checksum);
					map.put(checksum, att);

				} catch (Exception e) {

				}
			}

		}
		val queue = ThreadQueue.queue(10);
		for (final Attachment att : map.values()) {
			queue.command(new Runnable() {

				@Override
				public void run() {
					attachmentMap.put(att.getChecksum(), uploadAttachment(att));

				}
			});

		}
		queue.run(true);
		return attachmentMap;
	}

	@SneakyThrows
	private String uploadAttachment(Attachment att) {
		String u = cmd("attachments", "upload") + "&file="
				+ URLEncoder.encode(att.getName(), "UTF-8");
		URL url = new URL(u);
		String result = "";

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("PUT");
		connection.setRequestProperty("Content-Type", "multipart/form-data");
		connection.setRequestProperty("Content-Length",
				String.valueOf(att.getData().length));
		connection.setUseCaches(false);
		connection.setDoOutput(true);
		OutputStream outputStream = connection.getOutputStream();
		outputStream.write(att.getData());
		outputStream.flush();
		outputStream.close();
		result = retrieveResult(connection);

		return result;
	}

	private String checksum(Attachment att) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.update(att.getData());
		byte[] mdbytes = md.digest();

		// convert the byte to hex format
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < mdbytes.length; i++) {
			sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16)
					.substring(1));
		}
		return sb.toString();
	}

	public String cmd(String resource, String command) {
		return apiUrl.replaceAll("#resource#", resource)
				.replaceAll("#cmd#", command)
				.replaceAll("#username#", configuration.getUsername())
				.replaceAll("#apiKey#", configuration.getPassword());
	}

	@SneakyThrows
	public MailServerStatus retrieveServerStatus(String id) {
		String u = apiUrlStart + "/mailer/status/" + id
				+ "?showdelivered=true&showfailed=true&showpending=true&showopened=true";

		URL url = new URL(u);
		URLConnection conn = url.openConnection();
		String result = retrieveResult(conn);

		MailServerStatus status = MailServerStatus.builder()
				.status(getValue(result, "status"))
				.pending(getValue2(result, "pending"))
				.opened(getValue2(result, "opened"))
				.clicked(getValue2(result, "clicked"))
				.delivered(getValue2(result, "delivered"))
				.failed(getValue2(result, "failed")).build();
		return status;
	}

	private String getValue2(String status, String name) {
		String node = "<" + name + ">";
		int indexOf = status.indexOf(node);
		String retur = "";
		if (indexOf > 0) {

			retur = status.substring(indexOf + node.length());
			node = "<address>";
			indexOf = retur.indexOf(node);
			if (indexOf > 0) {
				retur = retur.substring(indexOf + node.length());
				retur = retur.substring(0, retur.indexOf("<"));
			} else {
				retur = "";
			}

		}
		return retur;
	}

	private String retrieveResult(URLConnection conn) throws IOException {
		BufferedReader rd = null;
		String result = "";
		try {
			rd = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));

			boolean cont = true;
			while (cont) {
				String line = rd.readLine();
				if (line != null) {
					result += line;

				} else {
					cont = false;
				}
			}

		} finally {
			if (rd != null) {
				rd.close();
			}
		}
		return result;
	}

	private String getValue(String status, String name) {
		String node = "<" + name + ">";
		int indexOf = status.indexOf(node);
		String retur = "";
		if (indexOf > 0) {
			retur = status.substring(indexOf + node.length());
			retur = retur.substring(0, retur.indexOf("<"));
		}
		return retur;
	}

	@SneakyThrows
	private void sendEmail(MailDTO mail) {
		String result = "";
		try {

			String replyTo = mail.getReplyTo();
			if (replyTo == null) {
				replyTo = mail.getFrom();
			}
			// Construct the data
			String _data = "userName="
					+ URLEncoder.encode(configuration.getUsername(), "UTF-8");
			_data += "&api_key="
					+ URLEncoder.encode(configuration.getPassword(), "UTF-8");
			_data += "&from=" + URLEncoder.encode(mail.getFrom(), "UTF-8");
			_data += "&from_name=" + URLEncoder.encode(mail.getFrom(), "UTF-8");
			_data += "&reply_to=" + URLEncoder.encode(replyTo, "UTF-8");
			_data += "&reply_to_name=" + URLEncoder.encode(replyTo, "UTF-8");
			_data += "&subject="
					+ URLEncoder.encode(mail.getSubject(), "UTF-8");
			_data += "&body_html=" + URLEncoder.encode(mail.getBody(), "UTF-8");

			if (mail.getNonHtmlBody() != null) {
				_data += "&body_text="
						+ URLEncoder.encode(mail.getNonHtmlBody(), "UTF-8");
			}

			if (mail.getAttachments() != null
					&& mail.getAttachments().size() > 0) {
				_data += "&attachments=";
				boolean first = true;
				for (Attachment att : mail.getAttachments()) {
					if (!first) {
						_data += ";";
					} else {
						first = false;
					}
					_data += uploadAttachment(att);
				}
			}

			// Send data
			String recipients = "";
			recipients += mail.getTo();
			if (mail.getCc() != null && !"".equals(mail.getCc())) {
				recipients += ";" + mail.getCc();

			}
			if (mail.getBcc() != null && !"".equals(mail.getBcc())) {
				recipients += ";" + mail.getBcc();
			}
			for (String recipient : recipients.split(";")) {
				if (recipient != null && !"".equals(recipient)) {
					String data = _data;
					data += "&to=" + recipient;
					URL url = new URL(cmd("mailer", "send"));
					URLConnection conn = url.openConnection();
					conn.setDoOutput(true);
					OutputStreamWriter wr = new OutputStreamWriter(
							conn.getOutputStream());
					wr.write(data);
					wr.flush();
					BufferedReader rd = new BufferedReader(
							new InputStreamReader(conn.getInputStream()));
					result = rd.readLine();
					wr.close();
					rd.close();
					String to = recipient;

					repository.create(MailStatus.builder().id(result).to(to)
							.subject(mail.getSubject())
							.objectId(mail.getObjectId())
							.context(mail.getContext())
							.serverStatus(retrieveServerStatus(result))
							.sentDate(new Date()).build());
				}
			}

		}

		catch (Exception e) {

			e.printStackTrace();
		}

	}

}
