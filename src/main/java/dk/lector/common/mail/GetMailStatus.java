package dk.lector.common.mail;

import java.util.Arrays;
import java.util.List;

import lombok.val;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GetMailStatus {
	@Autowired
	private MailStatusRepository repository;

	@Autowired
	private MailService service;

	private List<String> doneStatus = Arrays.asList(new String[] { "complete",
			"error" });

	private boolean checkState(String state){
		return state!=null && state.length()>0;
	}
	@RequestMapping(value = "/mail/status/{context}/{objectId}", method = RequestMethod.GET)
	public @ResponseBody
	List<MailStatus> saveStatus(@PathVariable("context") String context,
			@PathVariable("objectId") String objectId) {
		val retur = repository.getMailStatus(context, objectId);
		for (final MailStatus status : retur) {
		//	if (!doneStatus.contains(status.getServerStatus().getStatus())) {
				MailServerStatus serverStatus = service
						.retrieveServerStatus(status.getId());
				status.setServerStatus(serverStatus);
				if(checkState(serverStatus.getDelivered())){
					status.setFinalState("complete");
				}
				else if (checkState(serverStatus.getFailed())){
					status.setFinalState("failed");
					status.setFailedEmails(serverStatus.getFailed());
				}
				else if (checkState(serverStatus.getPending())){
					status.setFinalState("pending");
				}
				else if (checkState(serverStatus.getOpened())){
					status.setFinalState("opened");
				}
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						repository.save(status);
						
					}
				}).start();
				
			//}

		}
		return retur;
	}

}
