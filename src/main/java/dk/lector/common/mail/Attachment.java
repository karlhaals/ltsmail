package dk.lector.common.mail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Attachment {
	private byte[] data;
	private String name;
	private String checksum;
	private String attachmentId;

}
